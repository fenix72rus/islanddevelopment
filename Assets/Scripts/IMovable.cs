﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable
{
    float MoveSpeed { get; set; }

    void Move();

    void SetMoveTarget(Vector3 _target);

    void DisableControl();

    void EnableControl();
}
