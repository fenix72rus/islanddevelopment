﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Player player;

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out var hit))
            {
                if (hit.collider.TryGetComponent(out IInteractable interactable))
                {
                    player.SetInteraction(interactable);
                    return;
                }
                if (hit.collider.TryGetComponent(out Island island))
                {
                    player.SetMoveTarget(hit.point);
                    return;
                }
            }
        }
    }
}
