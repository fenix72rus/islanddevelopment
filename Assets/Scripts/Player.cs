﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour, IMovable
{
    public float MoveSpeed
    {
        get => moveSpeed;
        set
        {
            if (value < 0)
                value = 0;
            navMeshAgent.speed = value;
            moveSpeed = value;
        }
    }

    public Transform Transform { get => _transform; }

    [SerializeField] private float moveSpeed, stopDistance;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private NavMeshObstacle navMeshObstacle;
    [SerializeField] private Animator _animator;

    private IInteractable interaction;
    private Vector3 target;
    private Transform _transform;

    private void Awake()
    {
        _transform = this.transform;
        navMeshAgent.speed = moveSpeed;
    }

    private void Update()
    {
        if (target != Vector3.zero)
            Move();
    }

    public void Move()
    {
        if (navMeshAgent.enabled == false)
            return;

        navMeshAgent.SetDestination(target);

        if (Vector3.Distance(_transform.position, target) < stopDistance)
        {
            _animator.SetBool("run", false);
            if(interaction != null)
            {
                if (interaction.GameObject.TryGetComponent(out Resource resource))
                    _animator.SetTrigger("tr_pickup");
            }

            target = Vector3.zero;
        }
    }

    public void MakeInteraction()
    {
        interaction.Interaction();
        interaction = null;
    }

    public void SetMoveTarget(Vector3 _target)
    {
        target = _target;
        _animator.SetBool("run", true);
    }

    public void SetInteraction(IInteractable _interaction)
    {
        SetMoveTarget(_interaction.Position);
        interaction = _interaction;
    }

    public void DisableControl()
    {
        navMeshAgent.enabled = false;
        _animator.SetBool("run", false);
    }

    public void EnableControl()
    {
        navMeshAgent.enabled = true;
    }
}
