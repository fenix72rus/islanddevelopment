﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    Vector3 Position { get; }
    GameObject GameObject { get; }

    void Interaction();
}
