﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour, IInteractable
{
    public Vector3 Position { get => _position; }
    public GameObject GameObject { get => _gameObject; }

    public int resourceValue;

    private Vector3 _position;
    private GameObject _gameObject;
    private Transform _transform;

    private void Awake()
    {
        _gameObject = this.gameObject;
        _transform = this.transform;
        _position = this.transform.position;

        _transform.rotation = Quaternion.Euler(_transform.rotation.eulerAngles.x, Random.Range(0, 360), _transform.rotation.eulerAngles.z);
    }

    public void PickedUp()
    {
        Destroy(_gameObject);
    }

    public void Interaction()
    {
        PickedUp();
    }
}
