﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Vector3 movementOffset;
    [SerializeField] private Transform lookingTarget, movementTarget;

    private Transform _transform;

    private void Awake()
    {
        _transform = this.transform;
    }

    private void Update()
    {
        _transform.LookAt(lookingTarget);

        _transform.position = movementTarget.position + movementOffset;
    }
}
